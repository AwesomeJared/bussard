From: Logefad Rivoud
Subject: subnet client
To: soc_tana
Message-Id: 129f0b2b-c973-42b0-a2de-688b23e739e3
Content-Type: text/plain; charset=UTF-8

I'm curious to learn about protocols, so I'm thinking of writing my own subnet
client. It looks like the `ssh_get_connection` function on my ship can be used
to run commands and get the output back in a way I can use for my own programs,
but the output itself is pretty confusing.



From: Gigovecijum Louwat
Subject: Re: subnet client
To: soc_tana
Message-Id: db9f722a-8197-4468-a66a-74820f7d4aa1
In-Reply-To: 129f0b2b-c973-42b0-a2de-688b23e739e3
Content-Type: text/plain; charset=UTF-8

There's actually pretty decent description of the subnet protocol available
online if you use the subnet system's built-in help. Here's the encoded message
for the help command:

    d7:command4:helpe

That will tell you a fair bit about how to handle the decoding part. But you'll
also need to know about how to get messages; I think that would be something
like sending this and parsing the response to get a table of groups that contain
messages:

    d7:command6:groupe

You'd have an outer loop that lists all the groups, then an inner loop that
takes each group and lists all the messages, requesting each one and copying it
into your ship's `docs.mail[group]` table.

Just remember that the encoding/decoding of commands and responses is a separate
step from the action of fetching and storing the messages.



From: Logefad Rivoud
Subject: Re: (2) subnet client
To: soc_tana
Message-Id: c9c5a8c9-e240-43c0-95a0-db86021472d7
In-Reply-To: db9f722a-8197-4468-a66a-74820f7d4aa1
Content-Type: text/plain; charset=UTF-8

Cool; thanks. Here's what I figured out:

    local decode = ship.actions.bencode_decoder
    local encode_str = function (s) return #s .. ":" .. s end
    local encode = function(t) -- only works for simple k/v string tables
      local s = "d"
      for k,v in pairs(t) do s = s .. encode_str(k) .. encode_str(v) end
      return s .. "e"
    end

    subnet_sync = function()
       local connection = ssh_get_connection("subnet", subnet_password)
       if(not connection) then return "Could not connect." end

       local sub = function(command) return connection("subnet " .. command) end
       local g_resp = sub(encode({command="groups"}))

       for _,group in ipairs(decode(g_resp).groups) do
          ship.docs.mail[group] = ship.docs.mail[group] or {}
          local listing = sub(encode({command="list",group=group}))
          ship.docs.mail[group]._unread = {}

          for _,post in ipairs(decode(listing).posts) do
             local response = sub(encode({command="get", group=group, post=post}))
             ship.docs.mail[group][post] = decode(response).content
             ship.docs.mail[group]._unread[post] = true
          end
       end
       connection("logout")
    end

Be sure to set the `subnet_password` local to the proper value first, of course.



From: Ruvad Busaratut
Subject: Re: (3) subnet client
To: soc_tana
Message-Id: 4094624d-3053-4a7b-b37e-6343a58b4ff6
In-Reply-To: c9c5a8c9-e240-43c0-95a0-db86021472d7
Content-Type: text/plain; charset=UTF-8

Wait, where does the `decode' function here come from? I don't have
access to that on my machine.



From: Logefad Rivoud
Subject: Re: (4) subnet client
To: soc_tana
Message-Id: c1d66e92-03ea-4ad5-b14c-8bebefa333d3
In-Reply-To: 4094624d-3053-4a7b-b37e-6343a58b4ff6
Content-Type: text/plain; charset=UTF-8

Oh, that functionality comes from a bencode_decoder upgrade I bought; I
think they're for sale at least on Nee Soon Station and Warnabu Station.
Not cheap though.
