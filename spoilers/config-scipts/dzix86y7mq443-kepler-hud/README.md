This is a single-file version of my config that draws all the nice 
orbital helpers based on Kepler's Laws.

Orange disk is the Kepler-free zone where measures intended to fight
time step size issues visibly affect the behaviour in an unphysical way.

Green disks around the target are the spheres of influence, the external
one is the zone where the target gives the largest change in gravity
force per meter, the next one is approximately the orbital capture zone
and the innermost one is the zone where the target provides the 
strongest gravity force.

The green line is the approximate orbit around the natural orbit center.
The straight multi-coloured line is the relative speed w.r.t. the target
with colour segments corresponding to the distance covered in various
time intervals.
